package com.samsung_english.samsung_english_statistics.util

import com.samsung_english.samsung_english_statistics.dto.ResultDto
import org.dhatim.fastexcel.Workbook
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.function.Consumer

class MakeExcel {

    private fun writeWorkbook(consumer: Consumer<Workbook?>) {
        val os = ByteArrayOutputStream(   )
        val wb = Workbook(os, "com.samsung_english", "1.0")
        consumer.accept(wb)
        wb.finish()
        os.writeTo(FileOutputStream(File("samsung_statics.xlsx")))
    }

    fun parallelStreams(resultDto: Map<String, ResultDto>): Boolean {
        writeWorkbook { wb ->
            val ws = wb!!.newWorksheet("Sheet 1")
            getDatas(resultDto).withIndex().map {
                ws.value(it.index, 0, it.value.answerText)
                ws.value(it.index, 1, it.value.countRight)
                ws.value(it.index, 2, it.value.countWrong)
                ws.value(it.index, 3, it.value.count)
                ws.value(it.index, 4, it.value.ratio)
            }
        }
        return true
    }

    private fun getDatas(resultDto: Map<String, ResultDto>) : List<WriteCellValue> {
        val writeCellValues: MutableList<WriteCellValue> = mutableListOf()
        writeCellValues.add(WriteCellValue("answer_text", "count_right", "count_wrong", "count", "ratio"))
        resultDto.map {
            writeCellValues.add(WriteCellValue(it.key, it.value.countRight.toString(), it.value.countWrong.toString(), it.value.count.toString(), it.value.ratio.toString()))
        }

        return writeCellValues
    }

    data class WriteCellValue(var answerText: String, var countRight: String, var countWrong : String, var count : String, var ratio: String)

}