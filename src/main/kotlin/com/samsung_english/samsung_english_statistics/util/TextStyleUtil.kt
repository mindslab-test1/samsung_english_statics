package com.samsung_english.samsung_english_statistics.util

class TextStyleUtil {

    fun removeSpecialString(text: String): String {

        val regex = Regex("\\.|,|\\?|!")

        var returnText = regex.replace(text, "")

        if (returnText.contains("\'")) {

            if (returnText.contains("won\'t")) returnText.replace("won\'t", "will not")

            val apostropheIndex = returnText.indexOf("\'")
            val apostropheToSpaceIndex = returnText.substring(returnText.indexOf("\'")).indexOf(" ")

            if (apostropheToSpaceIndex == -1) return returnText

            val textShort = returnText.substring(apostropheIndex until apostropheIndex + apostropheToSpaceIndex)

            returnText = when (textShort) {
                "\'m" -> returnText.replace("\'m", " am")
                "\'s" -> returnText.replace("\'s", " is")
                "\'ll" -> returnText.replace("\'ll", " will")
                "\'re" -> returnText.replace("\'re", " are")
                "\'t" -> shortFromWithNot(returnText)
                "\'ve" -> returnText.replace("\'ve", " have")
                else -> IllegalArgumentException("does not match").toString()
            }
        }

        return returnText
    }

    private fun shortFromWithNot(returnText: String): String {
        val apostropheSubstringIndex = returnText.substring(0, returnText.indexOf("\'"))
        val apostropheIndex = returnText.indexOf("\'")

        val text = if (apostropheSubstringIndex.indexOf(" ") > 0) {
            when (apostropheSubstringIndex.substring(apostropheSubstringIndex.indexOf(" "), apostropheSubstringIndex.lastIndex + 1).trim()) {
                "can" -> returnText.replace("\'t", " not")
                else -> apostropheSubstringIndex.removeSuffix("n") + returnText.substring(apostropheIndex, returnText.lastIndex + 1).replace("\'t", " not")
            }
        } else {
            apostropheSubstringIndex.removeSuffix("n") + returnText.substring(apostropheIndex, returnText.lastIndex + 1).replace("\'t", " not")
        }

        return text

    }


}