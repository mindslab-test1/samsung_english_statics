package com.samsung_english.samsung_english_statistics.entity

import lombok.Data
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Table(name = "dl_evaluation_user_tb")
@Entity
@Data
class EvaluationUserTbEntity {

    @Id
    var evalId: Long? = null
    var userId: String? = null
    lateinit var userText: String
    lateinit var answerText: String
    var createdDtm: Date? = null
}