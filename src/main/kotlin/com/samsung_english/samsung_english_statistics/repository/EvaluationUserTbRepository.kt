package com.samsung_english.samsung_english_statistics.repository

import com.samsung_english.samsung_english_statistics.dto.StaticDateDto
import com.samsung_english.samsung_english_statistics.entity.EvaluationUserTbEntity
import org.springframework.data.repository.CrudRepository
import java.util.*

interface EvaluationUserTbRepository: CrudRepository<EvaluationUserTbEntity, Long> {

    fun findByEvalIdGreaterThanAndCreatedDtmBetween(evalId: Long, start: Date, end: Date): List<EvaluationUserTbEntity>

}