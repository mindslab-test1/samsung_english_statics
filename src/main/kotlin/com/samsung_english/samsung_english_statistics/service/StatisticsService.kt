package com.samsung_english.samsung_english_statistics.service

import com.samsung_english.samsung_english_statistics.dto.ResultDto
import com.samsung_english.samsung_english_statistics.dto.StaticDateDto
import com.samsung_english.samsung_english_statistics.entity.EvaluationUserTbEntity
import com.samsung_english.samsung_english_statistics.repository.EvaluationUserTbRepository
import com.samsung_english.samsung_english_statistics.util.MakeExcel
import com.samsung_english.samsung_english_statistics.util.TextStyleUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileInputStream
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.servlet.http.HttpServletResponse


@Service
class StatisticsService {

    var resultMap: MutableMap<String, ResultDto> = mutableMapOf()

    @Autowired
    lateinit var evaluationUserTbRepository: EvaluationUserTbRepository

    fun getFindTexts(date: StaticDateDto, response: HttpServletResponse): Boolean {

        val format = SimpleDateFormat("yyyyMMdd")

        date.start = format.parse(date.startDate)
        date.end = format.parse(date.endDate)

        evaluationUserTbRepository.findByEvalIdGreaterThanAndCreatedDtmBetween(date.evalId, date.start, date.end).map {

            val key = resultMap.keys.contains(it.answerText)

            if (key) {
                countResult(it, resultMap, TextStyleUtil().removeSpecialString(it.userText).equals(TextStyleUtil().removeSpecialString(it.answerText), true))
            } else {
                resultMap[it.answerText] = ResultDto()
                countResult(it, resultMap, TextStyleUtil().removeSpecialString(it.userText).equals(TextStyleUtil().removeSpecialString(it.answerText), true))
            }
        }

        resultMap.map {
            it.value.ratio = it.value.countWrong.toDouble() / it.value.count * 100
        }

        val sortByRatio = resultMap.toList().sortedByDescending { (_, value) -> value.ratio }.toMap()

        if (MakeExcel().parallelStreams(sortByRatio)) {
            return true
        }
        return false
    }

    fun countResult(evaluationUser: EvaluationUserTbEntity, resultDto: MutableMap<String, ResultDto>, thisRight: Boolean) {

        resultDto[evaluationUser.answerText]!!.count += 1

        if (thisRight) resultDto[evaluationUser.answerText]!!.countRight += 1 else resultDto[evaluationUser.answerText]!!.countWrong += 1

    }

    fun getFile(response: HttpServletResponse) {
        val fileName = "samsung_statics.xlsx"
        val file = File("samsung_statics.xlsx")

        response.setHeader("Content-Disposition", "attachment; filename=$fileName;")
        response.setHeader("Content-Transfer-Encoding", "binary")
        response.setHeader("Content-Length", file.length().toString())
        response.setHeader("Pragma", "no-cache;")
        response.setHeader("Expires", "-1;")

        try {
            FileInputStream(file).use { fis ->
                response.outputStream.use { out ->
                    var readCount = 0
                    val buffer = ByteArray(1024)
                    while (fis.read(buffer).also { readCount = it } != -1) {
                        out.write(buffer, 0, readCount)
                    }
                }
            }
        } catch (ex: Exception) {
            throw RuntimeException("file Save Error")
        }
    }

}