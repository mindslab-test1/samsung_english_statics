package com.samsung_english.samsung_english_statistics

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SamsungEnglishStatisticsApplication

fun main(args: Array<String>) {
    runApplication<SamsungEnglishStatisticsApplication>(*args)
}
