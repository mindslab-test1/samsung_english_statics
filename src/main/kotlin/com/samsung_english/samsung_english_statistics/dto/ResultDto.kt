package com.samsung_english.samsung_english_statistics.dto

import lombok.Data

@Data
class ResultDto {

    var countRight: Int = 0
    var countWrong: Int = 0
    var count: Int = 0
    var ratio: Double? = null

}