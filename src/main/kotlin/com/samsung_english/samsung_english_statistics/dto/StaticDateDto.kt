package com.samsung_english.samsung_english_statistics.dto

import lombok.Data
import java.util.*

@Data
class StaticDateDto {

    var evalId: Long = 0
    lateinit var startDate: String
    lateinit var endDate: String
    lateinit var start: Date
    lateinit var end: Date

}