package com.samsung_english.samsung_english_statistics.enum

enum class TextShortForm(val text: String) {
    ARE("\'re"),
    WILL("\'ll"),
    IS("\'s"),
    NOT("\'t"),
}