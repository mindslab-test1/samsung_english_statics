package com.samsung_english.samsung_english_statistics.contoller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@RequestMapping("")
@Controller
class MainController {

    @RequestMapping("")
    fun default(): String {

        return "/main"

    }


    @RequestMapping("/main")
    fun main() {

    }

}