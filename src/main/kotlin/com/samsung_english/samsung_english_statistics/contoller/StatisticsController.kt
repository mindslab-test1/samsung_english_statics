package com.samsung_english.samsung_english_statistics.contoller

import com.samsung_english.samsung_english_statistics.dto.StaticDateDto
import com.samsung_english.samsung_english_statistics.service.StatisticsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.function.RequestPredicates.contentType
import java.io.File
import java.io.FileInputStream
import javax.servlet.http.HttpServletResponse


@RestController
class StatisticsController {

    @Autowired
    lateinit var statisticsService: StatisticsService

    @RequestMapping("/statics")
    fun getAnswerStatistics(@RequestBody date: StaticDateDto, response: HttpServletResponse) {

        statisticsService.getFindTexts(date, response)

    }

    @RequestMapping("/get_file")
    fun test(response: HttpServletResponse) {
        statisticsService.getFile(response)
    }
}